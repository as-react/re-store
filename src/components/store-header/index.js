import React from 'react';
import './store-header.scss';
import { Link } from 'react-router-dom';
const StoreHeader = ({ numItems, total }) => {
  return (
    <nav className='navbar store-header row'>
      <Link to='/'>
        <div className='navbar-brand logo text-dark'>ReStore</div>
      </Link>

      <Link to='/cart'>
        <div className='nav-link shopping-cart'>
          <i className='cart-icon fa fa-shopping-cart' />
          {numItems} items (${total})
        </div>
      </Link>
    </nav>
  );
};

export default StoreHeader;
