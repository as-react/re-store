import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { HomePage, CartPage } from '../pages';
import StoreHeader from '../store-header';

const App = () => {
  return (
    <main role='main' className='container'>
      <StoreHeader numItems={5} total={130} />
      <Switch>
        <Route path='/' exact component={HomePage} />
        <Route path='/cart' exact component={CartPage} />
      </Switch>
    </main>
  );
};

export default App;
