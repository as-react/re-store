import React from 'react';
import './spinner.scss';

const Spinner = () => {
  return (
    <div className='loader'>
      <div className='face'>
        <div className='circle'></div>
      </div>
      <div className='face'>
        <div className='circle'></div>
      </div>
    </div>
  );
};

export default Spinner;
