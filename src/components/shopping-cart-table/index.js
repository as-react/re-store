import React from 'react';
import './shopping-cart-table.scss';
import { connect } from 'react-redux';
import {
  bookAddedToCart,
  bookRemovedFromCart,
  bookDecreaseFromCart,
} from '../../actions';

const ShoppingCartTable = ({
  items,
  total,
  onIncrease,
  onDecrease,
  onDelete,
}) => {
  const renderRow = (item, idx) => {
    const { id, title, count, total } = item;
    return (
      <tr key={id}>
        <td>{idx + 1}</td>
        <td>{title}</td>
        <td>{count}</td>
        <td>${total}</td>
        <td>
          <button
            onClick={() => onDelete(id)}
            className='btn btn-outline-danger btn-sm'>
            <i className='fa fa-trash-o'></i>
          </button>
          <button
            onClick={() => onIncrease(id)}
            className='btn btn-outline-success btn-sm'>
            <i className='fa fa-plus-circle'></i>
          </button>
          <button
            onClick={() => onDecrease(id)}
            className='btn btn-outline-warning btn-sm'>
            <i className='fa fa-minus-circle'></i>
          </button>
        </td>
      </tr>
    );
  };

  return (
    <div className='shopping-cart-table'>
      <h2>Your order</h2>
      <table className='table'>
        <thead>
          <tr>
            <th>#</th>
            <th>Item</th>
            <th>Count</th>
            <th>Price</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>{items.map(renderRow)}</tbody>
      </table>
      <div className='total'>Total: ${total}</div>
    </div>
  );
};

const mapStateToProps = ({ shoppingCart: { cartItems, orderTotal } }) => {
  return {
    items: cartItems,
    total: orderTotal,
  };
};

// const mapDispatchToProps = (dispatch) => {
//   return {
//     onIncrease: (id) => {
//       dispatch(bookAddedToCart(id));
//     },
//     onDecrease: (id) => {
//       dispatch(bookDecreaseFromCart(id));
//     },
//     onDelete: (id) => {
//       dispatch(bookRemovedFromCart(id));
//     },
//   };
// };

const mapDispatchToProps = {
  onIncrease: bookAddedToCart,
  onDecrease: bookDecreaseFromCart,
  onDelete: bookRemovedFromCart,
};

export default connect(mapStateToProps, mapDispatchToProps)(ShoppingCartTable);
