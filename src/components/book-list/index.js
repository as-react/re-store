import React, { Component } from 'react';
import { connect } from 'react-redux';
import './book-list.scss';

import { withBookStoreService } from '../hoc';
import { fetchBooks, bookAddedToCart } from '../../actions';
import { compose } from '../../utils';
import BookListItem from '../book-list-item';
import Spinner from '../spinner';
import ErrorIndicator from '../error-indicator';
import { bindActionCreators } from 'redux';

const BookList = ({ books, onAddedToCart }) => {
  return (
    <ul className='book-list'>
      {books.map((book) => {
        return (
          <li key={book.id}>
            <BookListItem
              book={book}
              onAddedToCart={() => onAddedToCart(book.id)}
            />
          </li>
        );
      })}
    </ul>
  );
};

class BookListContainer extends Component {
  componentDidMount() {
    // 1. receive data
    // 2. dispatch action to store
    // const {
    //   bookstoreService,
    //   booksLoaded,
    //   booksRequested,
    //   booksError,
    // } = this.props;

    // booksRequested();
    // bookstoreService
    //   .getBooks()
    //   .then((data) => booksLoaded(data))
    //   .catch((error) => booksError(error));

    this.props.fetchBooks();
  }

  render() {
    const { books, loading, error, onAddedToCart } = this.props;

    if (loading) {
      return <Spinner />;
    }

    if (error) {
      return <ErrorIndicator />;
    }

    return <BookList books={books} onAddedToCart={onAddedToCart} />;
  }
}

const mapStateToProps = ({ bookList: { books, loading, error } }) => {
  return { books, loading, error };
};

const mapDispatchToProps = (dispatch, { bookstoreService }) => {
  return bindActionCreators({
    fetchBooks: fetchBooks(bookstoreService),
    onAddedToCart: bookAddedToCart,
  }, dispatch);
};

// const mapDispatchToProps = (dispatch, { bookstoreService }) => {
//   return {
//     // fetchBooks: fetchBooks(dispatch, bookstoreService),
//     fetchBooks: () => dispatch(fetchBooks(bookstoreService)()),
//     onAddedToCart: (id) => dispatch(bookAddedToCart(id)),
//   };
// };

// const mapDispatchToProps = (dispatch, ownProps) => {
//   const { bookstoreService } = ownProps;
//   return {
//     fetchBooks: () => {
//       dispatch(booksRequested());
//       bookstoreService
//         .getBooks()
//         .then((data) => dispatch(booksLoaded(data)))
//         .catch((error) => dispatch(booksError(error)));
//     },
//   };
// };

// const mapDispatchToProps = {
//   booksLoaded,
//   booksRequested,
//   booksError,
// };

// const mapDispatchToProps = (dispatch) => {
// return bindActionCreators({booksLoaded}, dispatch);
// return {
//   booksLoaded: (newBooks) => {
//     dispatch(booksLoaded(newBooks));
//   },
// };
// };

export default compose(
  withBookStoreService(),
  connect(mapStateToProps, mapDispatchToProps)
)(BookListContainer);
